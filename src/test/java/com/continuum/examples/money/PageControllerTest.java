package com.continuum.examples.money;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

class PageControllerTest {
    @Test
    void german() {
        NumberFormat instance = NumberFormat.getInstance(new Locale("en", "IN"));
        System.out.println(instance.format(1_000_000));
    }

    @Test
    void roundingMode() {
        BigDecimal bigDecimal = BigDecimal.TEN.setScale(2, RoundingMode.HALF_EVEN);
        System.out.println(bigDecimal.multiply(BigDecimal.valueOf(3.3345)).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    void india() {
        MonetaryAmountFormat format = MonetaryFormats.getAmountFormat(new Locale("en, IN"));
        System.out.println(format.format(Money.of(BigDecimal.valueOf(123456789), "INR")));
    }

}
