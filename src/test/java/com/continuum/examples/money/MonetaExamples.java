package com.continuum.examples.money;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.AmountFormatParams;
import org.javamoney.moneta.format.CurrencyStyle;
import org.javamoney.moneta.function.MonetaryFunctions;
import org.junit.jupiter.api.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.MonetaryException;
import javax.money.convert.*;
import javax.money.format.AmountFormatQuery;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MonetaExamples {

    private static final CurrencyUnit EURO = Monetary.getCurrency("EUR");

    @Test
    void money() {
        var simple = Money.of(10, "EUR");
        var currency = Money.of(10, Monetary.getCurrency("EUR"));
        var bigDecimal = Money.of(BigDecimal.TEN, Monetary.getCurrency("EUR"));
        System.out.println("simple = " + simple);
        System.out.println("currency = " + currency);
        System.out.println("bigDecimal = " + bigDecimal);
    }

    @Test
    void currencySelection() {
        var us = Monetary.getCurrency(Locale.US);
        System.out.println("USA = " + us);

        var usd = Monetary.getCurrency("USD");
        System.out.println("USD = " + usd);

        var greatBritain = Monetary.getCurrency(new Locale("en", "GB"));
        System.out.println("Great Britain = " + greatBritain);

        var belgium = Monetary.getCurrency(new Locale("nl", "BE"));
        System.out.println("Belgium = " + belgium);
    }

    @Test
    void commonOperations() {
        Money euro10 = Money.of(10, "EUR");
        Money euro5 = Money.of(10, "EUR");

        var addition = euro5.add(euro10);
        System.out.println("10 EUR + 5 EUR = " + addition);

        var subtraction = euro10.subtract(euro5);
        System.out.println("10 EUR - 5 EUR = " + subtraction);

        var multiplication = euro10.multiply(1.21);
        System.out.println("10 EUR * 1.21 = " + multiplication);

        var division = euro10.divide(10);
        System.out.println("10 EUR / 10 = " + division);

        var comparison = euro10.isLessThan(euro5);
        System.out.println("10 < 5 = " + comparison);
    }


    private Stream<MonetaryAmount> getStream() {
        Money euro10 = Money.of(10, "EUR");
        Money euro5 = Money.of(10, "EUR");
        Money euro1 = Money.of(1, "EUR");
        Money dollar = Money.of(1, "USD");

        return Stream.of(euro1, euro5, euro10, dollar);
    }

    @Test
    void steamingOperations() {
        System.out.println(getStream().collect(MonetaryFunctions.groupBySummarizingMonetary()));

        MonetaryAmount sum = getStream().filter(MonetaryFunctions.isCurrency(EURO))
                .reduce(Money.of(0, "EUR"), MonetaryFunctions.sum());
        System.out.println("sum = " + sum);
        MonetaryAmount max = getStream().filter(MonetaryFunctions.isCurrency(EURO)).reduce(Money.of(0, "EUR"), MonetaryFunctions.max());
        System.out.println("max = " + max);
    }

    @Test
    void conversion() {
        CurrencyConversion currencyConversion = MonetaryConversions.getExchangeRateProvider("IMF").getCurrencyConversion("EUR");
        Money gbp = Money.of(10, "GBP");
        System.out.println(gbp.with(currencyConversion));
    }

    @Test
    void conversionPast() {
        ConversionQuery query = ConversionQueryBuilder.of()
                .setTermCurrency("EUR")
                .set(LocalDate.of(2005, 5, 10))
                .build();
        ExchangeRateProvider exchangeRateProvider = MonetaryConversions.getExchangeRateProvider(query);
        Money gbp = Money.of(10, "GBP");
        System.out.println(gbp.with(exchangeRateProvider.getCurrencyConversion("EUR")));
    }

    @Test
    void brexit() {
        ExchangeRateProvider exchangeRateProvider = MonetaryConversions.getExchangeRateProvider("ECB-HIST");

        List<Money> result = IntStream.range(2012, 2019)
                .mapToObj(i -> convertInYear(i, exchangeRateProvider))
                .collect(Collectors.toList());
        result.forEach(System.out::println);

    }

    private Money convertInYear(int i, ExchangeRateProvider exchangeRateProvider) {
        ConversionQuery query = ConversionQueryBuilder.of()
                .setTermCurrency("EUR")
                .set(new LocalDate[]{LocalDate.of(i, 1, 2), LocalDate.of(i, 1, 3), LocalDate.of(i, 1, 4)})
                .build();
        CurrencyConversion currencyConversion = MonetaryConversions.getExchangeRateProvider(query).getCurrencyConversion(query);
        return Money.of(10, "GBP").with(currencyConversion);
    }

    @Test
    void streamingOperationDifferentCurrencies() {
        assertThatThrownBy(() -> getStream().reduce(Money.of(0, "EUR"), MonetaryFunctions.sum()))
                .isInstanceOf(MonetaryException.class);
    }


    @Test
    void addingDifferentCurrencies() {
        System.out.println(getStream()
                .reduce(Money.of(0, "EUR"), MonetaryFunctions.sum(MonetaryConversions.getExchangeRateProvider("IMF"), EURO)));
    }

    @Test
    void formattingUsa() {
        MonetaryAmountFormat usFormatter = MonetaryFormats.getAmountFormat(
                AmountFormatQueryBuilder.of(Locale.US)
                        .set(CurrencyStyle.SYMBOL)
                        .build()
        );
        MonetaryAmountFormat germanFormatter = MonetaryFormats.getAmountFormat(
                AmountFormatQueryBuilder.of(Locale.GERMANY)
                        .set(CurrencyStyle.SYMBOL)
                        .build()
        );

        MonetaryAmountFormat belgianFormatter = MonetaryFormats.getAmountFormat(
                AmountFormatQueryBuilder.of(new Locale("nl", "BE"))
                        .set(CurrencyStyle.SYMBOL)
                        .build()
        );
        MonetaryAmountFormat frenchFormatter = MonetaryFormats.getAmountFormat(
                AmountFormatQueryBuilder.of(Locale.FRANCE)
                        .set(CurrencyStyle.SYMBOL)
                        .build()
        );


        Money amount = Money.of(1093838488, "USD");
        String usResult = usFormatter.format(amount);
        String germanResult = germanFormatter.format(amount);
        String belgianResult = belgianFormatter.format(amount);
        String frenchResult = frenchFormatter.format(amount);
        System.out.println("usResult = " + usResult);
        System.out.println("germanResult = " + germanResult);
        System.out.println("belgianResult = " + belgianResult);
        System.out.println("frenchResult = " + frenchResult);
    }


    @Test
    void indian() {

        Money amt = Money.of(123412341234.5678, "INR");
        System.out.println(MonetaryFormats.getAmountFormat(new Locale("", "INR")).format(amt));

        // no with adaptive groupings
        AmountFormatQuery indianFormat = AmountFormatQueryBuilder.of(new Locale("", "INR"))
                .set(AmountFormatParams.GROUPING_SIZES, new int[]{2, 3})
                .set(AmountFormatParams.GROUPING_GROUPING_SEPARATORS, new char[]{',', '`'})
                .build();
        System.out.println(MonetaryFormats.getAmountFormat(
                indianFormat)
                .format(amt));

        AmountFormatQuery indianFormat2 = AmountFormatQueryBuilder.of(new Locale("", "INR"))
                .set(AmountFormatParams.PATTERN, "#,##0.### ¤")
                .build();
        System.out.println(MonetaryFormats.getAmountFormat(indianFormat2)
                .format(amt));
    }

}
