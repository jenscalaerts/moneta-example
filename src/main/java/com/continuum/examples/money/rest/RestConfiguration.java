package com.continuum.examples.money.rest;

import com.fasterxml.jackson.databind.Module;
import org.javamoney.moneta.format.CurrencyStyle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.zalando.jackson.datatype.money.MoneyModule;

import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryFormats;

@Configuration
public class RestConfiguration {
    @Bean
    public Module monetaModule() {
        return new MoneyModule().withFormatting(
                locale -> MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.of(LocaleContextHolder.getLocale())
                        .set(CurrencyStyle.SYMBOL)
                        .build()));
    }

}
