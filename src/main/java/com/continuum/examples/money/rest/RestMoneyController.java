package com.continuum.examples.money.rest;

import com.continuum.examples.money.OrderLine;
import com.continuum.examples.money.OrderService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Locale;

@RestController
public class RestMoneyController {
    private final OrderService orderService;

    public RestMoneyController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderLine> index(Locale locale) {
        return orderService.getOrderLinesForCountry(locale);
    }
}
