package com.continuum.examples.money;

import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class OrderLine {
    private final String description;
    private final Money amount;

    public OrderLine(String description, Money of) {
        this.description = description;
        this.amount = of;

    }

    public String getDescription() {
        return description;
    }

    public Money getAmount() {
        return amount;
    }

    public Money getVat() {
        return getAmount()
                .multiply(BigDecimal.valueOf(0.21));
    }

    public Money getTotalAmount() {
        return getAmount().add(getVat());
    }

}
