package com.continuum.examples.money.freemarker;

import com.continuum.examples.money.OrderLine;
import com.continuum.examples.money.OrderService;
import org.javamoney.moneta.Money;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.money.Monetary;
import java.util.List;
import java.util.Locale;

@Controller
public class PageController {
    private final OrderService orderService;

    public PageController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/")
    public ModelAndView index(Model model, Locale locale) {

        List<OrderLine> orderLines = orderService.getOrderLinesForCountry(locale);
        return new ModelAndView("index")
                .addObject("orderLines", orderLines)
                .addObject("total", orderLines.stream().map(OrderLine::getTotalAmount).reduce(Money.zero(Monetary.getCurrency(locale)), Money::add))
                .addObject("locale", locale);
    }
}
