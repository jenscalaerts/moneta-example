package com.continuum.examples.money.freemarker;

import freemarker.template.*;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.CurrencyStyle;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.expression.spel.ast.StringLiteral;

import javax.money.Monetary;
import javax.money.format.AmountFormatQuery;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryFormats;
import java.util.Locale;

public class MoneyWrapper extends DefaultObjectWrapper {
    public MoneyWrapper() {
        super();
    }

    @Override
    protected TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
        if (obj instanceof Money) {
            return new SimpleScalar(formatMoney((Money) obj));
        }
        return super.handleUnknownType(obj);
    }

    private String formatMoney(Money money) {
        return MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.of(LocaleContextHolder.getLocale())
                .set(CurrencyStyle.SYMBOL)
                .build()).format(money);
    }
}
