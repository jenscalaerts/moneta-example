package com.continuum.examples.money;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final CurrencyUnit EUR = Monetary.getCurrency("EUR");
    private final ExchangeRateProvider exchangeRateProvider;

    public OrderService(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public List<OrderLine> getOrderLinesForCountry(Locale countryCode) {
        var orderLines = Arrays.asList(
                new OrderLine("Mouse", Money.of(BigDecimal.valueOf(75.25), EUR)),
                new OrderLine("Keyboard", Money.of(BigDecimal.valueOf(75.25), EUR)),
                new OrderLine("Massive Desktop", Money.of(BigDecimal.valueOf(1500), EUR)),
                new OrderLine("Server", Money.of(BigDecimal.valueOf(1_000_000), EUR))
        );

        CurrencyUnit localCurrency = Monetary.getCurrency(countryCode);
        if (!localCurrency.equals(EUR)) {
            CurrencyConversion currencyConversion = exchangeRateProvider.getCurrencyConversion(localCurrency);
            return orderLines.stream()
                    .map( i -> new OrderLine(i.getDescription(), i.getAmount().with(currencyConversion)))
                    .collect(Collectors.toList());
        }
        return orderLines;
    }


}
